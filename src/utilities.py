import satellite_network_protocol.src as snp

def buildStdPackage(
        payload,
        payloadType,
        senderID,
        senderType,
        messageID,
        number,
        last,
        recvID=65535):

    if not type(payload) is bytes:
        raise TypeError('buildPackage() only takes in bytes as payload!')

    if len(payload) > snp.package.PAYLOAD_SIZE:
        raise ValueError('Payload is too long to fit into one frame!')

    pkg = snp.package.Package()

    pkg.senderID = senderID
    pkg.recvID = recvID

    pkg.isControl = False
    pkg.isLast = last
    pkg.senderType = senderType
    pkg.payloadType = payloadType
    pkg.messageID = messageID
    pkg.number = number
    pkg.payload = payload

    byteMessage = pkg.toByteMessage()

    return byteMessage


def buildCtrlPackage(ctrlMessageType, arg=b'', senderID=0, recvID=0):
    pkg = snp.package.Package()
    pkg.configAsCtrl(ctrlMessageType, arg, senderID, recvID)
    return pkg.toByteMessage()


def buildCtrlConnRefused(reason):
    if not type(reason) is bytes:
        reason = bytes(reason, 'utf-8')
    if len(reason) > 122:
        reason = reason[:122]

    return buildCtrlPackage(
            ctrlMessageType=0,
            arg=reason)


def buildCtrlClientID(clientID):
    return buildCtrlPackage(
            ctrlMessageType=1, 
            arg=clientID.to_bytes(length=2, byteorder='big'), 
            senderID=0,
            recvID=clientID)

def buildCtrlConnReq(token):
    return buildCtrlPackage(
            ctrlMessageType=2,
            arg=bytes(token, 'utf-8'))
